/**
 * 
 */
package book20_javabean;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * @author 彭茂昌
 * @since 2018年4月5日 上午10:33:22
 * 
 */
public class DBAccess {
	// 数据库驱动
	private String drv = "com.mysql.jdbc.Driver";
	// 数据库地址
	private String url = "jdbc:mysql://localhost:3306/demo";
	// 用户名
	private String usr = "root";
	// 密码
	private String pwd = "password";
	// 数据库连接对象
	private Connection conn = null;
	// 数据库声明对象
	private Statement stm = null;
	// 数据库结果集
	private ResultSet rs = null;

	public String getDrv() {
		return drv;
	}

	public void setDrv(String drv) {
		this.drv = drv;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getUsr() {
		return usr;
	}

	public void setUsr(String usr) {
		this.usr = usr;
	}

	public String getPwd() {
		return pwd;
	}

	public void setPwd(String pwd) {
		this.pwd = pwd;
	}

	public Connection getConn() {
		return conn;
	}

	public void setConn(Connection conn) {
		this.conn = conn;
	}

	public Statement getStm() {
		return stm;
	}

	public void setStm(Statement stm) {
		this.stm = stm;
	}

	public ResultSet getRs() {
		return rs;
	}

	public void setRs(ResultSet rs) {
		this.rs = rs;
	}

	public boolean creatConn() {
		boolean b = false;
		try {
			Class.forName(drv).newInstance();
			conn = DriverManager.getConnection(url, usr, pwd);
			b = true;
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return b;
	}

	public boolean update(String sql) {
		boolean b = false;
		try {
			stm = conn.createStatement();
			stm.execute(sql);
			b = true;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return b;
	}

	public void query(String sql) {
		try {
			stm = conn.createStatement();
			rs = stm.executeQuery(sql);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public boolean next() {
		boolean b = false;
		try {
			b = rs.next();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return b;
	}

	public String getValue(String field) {
		String value = null;
		try {
			if (rs.next())
				value = rs.getString(field);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return value;
	}

	public void closeRs() {
		try {
			if(rs!=null)
				rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	public void closeStm() {
		try {
			if(stm!=null)
				stm.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	public void closeConn() {
		try {
			if(conn!=null)
				conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
